# --- !Ups

insert into "STUDENTS" values (1,'kenny','kenny@abc.com');
insert into "STUDENTS" values (2,'james','james@def.com');
insert into "STUDENTS" values (3,'rose','rose@xyz.com');


# --- !Downs

truncate table "STUDENTS";
