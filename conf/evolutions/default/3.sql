# --- !Ups

create or replace function "CFN_GET_STUDENT_BY_ID"(
  "_ID" int
) returns table(
  "NAME" varchar(255),
  "EMAIL" varchar(255)
) as 
$BODY$
begin
  return query
  select std."NAME", std."EMAIL" from "STUDENTS" as std 
  where "ID" = "_ID";;
end;;
$BODY$
LANGUAGE plpgsql;

# --- !Downs

drop function "CFN_GET_STUDENT_BY_ID"(int);