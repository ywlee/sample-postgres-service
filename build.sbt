name := """sample-postgres-service"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.11"

libraryDependencies ++= Seq(
  //jdbc, //this should be removed, otherwise it will have runtime error
  ws,
  "org.postgresql" % "postgresql" % "42.1.1",
  "com.typesafe.play" %% "play-slick" % "2.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.0",
  "org.typelevel" %% "cats" % "0.9.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.1" % Test
)
