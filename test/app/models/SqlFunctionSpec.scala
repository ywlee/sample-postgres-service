package models

import play.api.test._
import play.api.test.Helpers._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject.bind
import play.api.Application
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import cats.implicits._
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import org.scalatest._
import play.api.db.evolutions._

class SqlFunctionSpec extends PlaySpec
  with OneAppPerSuite
  with ScalaFutures
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  implicit override lazy val app = new GuiceApplicationBuilder().
    configure(
      "slick.dbs.default.driver" -> "slick.driver.PostgresDriver$",
      "slick.dbs.default.db.driver" -> "org.postgresql.Driver",
      "slick.dbs.default.db.url" -> "jdbc:postgresql://postgres/test",
      "slick.dbs.default.db.user" -> "test",
      "slick.dbs.default.db.password" -> "",
      "slick.dbs.default.db.connectionTimeout" -> "30 seconds",
      "slick.dbs.default.db.keepAliveConnection" -> true).build

  def sqlExecutor(implicit app: Application): models.SqlExecutor = 
    Application.instanceCache[models.SqlExecutor].apply(app)

  override def beforeEach() {
    whenReady(sqlExecutor.truncate) { res => 
      println("cleanup")
    }
  }

  override def afterEach() {
    whenReady(sqlExecutor.truncate) { res => 
      println("cleanup")
    }
  }

  override def beforeAll() = {
    OfflineEvolutions.applyScript(
      new java.io.File("."),
      this.getClass.getClassLoader,
      app.injector.instanceOf[play.api.db.DBApi],
      "default",
      true
    )
  }

  "CFN_GET_STUDENT_BY_ID.sql" should {
    "find by id" in {
      whenReady(sqlExecutor.initDb) { r =>
        val id:Int = 5
        whenReady(sqlExecutor.getStudent(id)) { res =>
          assert(res(0)._1 == "ywlee")
          assert(res(0)._2 == "ywlee@abc.com")
        }
      }
    }
  }
}