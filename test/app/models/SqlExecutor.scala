package models

import play.api.db.slick._
import javax.inject._
import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global

@Singleton
class SqlExecutor @Inject()(
    protected val dbConfigProvider: DatabaseConfigProvider,
    protected implicit val ec: ExecutionContext)
  extends HasDatabaseConfigProvider[slick.driver.JdbcProfile] {
  import driver.api._

  def truncate = {
    db.run(sql"""truncate table "STUDENTS" cascade; """.as[Int])
  }

  def initDb = {
    db.run(sql"""insert into "STUDENTS" values (5,'ywlee','ywlee@abc.com');""".as[Int])
  }

  def getStudent(id: Int) = {
    db.run(sql"""
      select "NAME", "EMAIL" 
      from "CFN_GET_STUDENT_BY_ID"($id);""".as[(String, String)])
  }
}