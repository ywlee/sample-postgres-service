package models.repo

import java.util.UUID
import javax.inject.{ Inject, Singleton }
import scala.concurrent.{ Future, ExecutionContext }
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import cats.data.OptionT
import models.domain.Student
import slick.profile.RelationalProfile

@Singleton
class StudentRepo @Inject()(
    dao: models.dao.StudentDao,
    implicit val ec: ExecutionContext,
    protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[slick.driver.JdbcProfile] {
  import driver.api._

  def findByName(name: String): OptionT[Future, Student] = {
    val res = dao.query.filter(_.name.trim.toUpperCase === name.trim.toUpperCase).result
    OptionT(db.run(res.headOption))
  }

  def findByEmail(email: String): OptionT[Future, Student] = {
    val res = dao.query.filter(_.email === email).result
    OptionT(db.run(res.headOption))
  }
}
