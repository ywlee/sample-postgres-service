package models.dao

import javax.inject.{ Inject, Singleton }
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import models.domain.Student
import slick.profile.RelationalProfile

@Singleton
class StudentDao @Inject()(
    protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[slick.driver.JdbcProfile] {
  import driver.api._

  protected class StudentTbl(tag: Tag)
    extends Table[Student](tag, "STUDENTS") {
    def id = column[Int]("ID")
    def name = column[String]("NAME", O.Length(255, true))
    def email = column[String]("EMAIL", O.Length(255, true))
    
    def * = (id, name, email) <> (Student.tupled, Student.unapply)
  }

  val query = TableQuery[StudentTbl]
}
