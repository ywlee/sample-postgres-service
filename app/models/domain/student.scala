package models.domain

case class Student(
  id: Int,
  name: String,
  email: String)
