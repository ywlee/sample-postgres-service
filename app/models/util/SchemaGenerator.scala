package models.util

import javax.inject.{ Inject, Singleton }
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import models.dao._

@Singleton
class SchemaGenerator @Inject()(
  studentDao: StudentDao,
  val dbConfigProvider: DatabaseConfigProvider)
extends HasDatabaseConfigProvider[slick.driver.JdbcProfile] {
  import driver.api._

  def createDDLScript() = {
    val schemas = studentDao.query.schema 
    val writer = new java.io.PrintWriter("target/schema.sql")
    writer.write("# --- !Ups\n\n")
    schemas.createStatements.foreach { s => writer.write(s + ";\n\n") }

    writer.write("\n\n# --- !Downs\n\n")
    schemas.dropStatements.foreach { s => writer.write(s + ";\n") }

    println("Schema definitions are written")

    writer.close()
  }

  createDDLScript()
}
