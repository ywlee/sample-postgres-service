package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent._
import cats.implicits._
import models.repo._

@Singleton
class HomeController @Inject()(
  val myRepo: StudentRepo,
  implicit val ec: ExecutionContext
) extends Controller {
  def index = Action {
    Ok("ok...")
  }

  def findByEmail(mail: String) = Action.async { implicit req =>
    myRepo.findByEmail(mail) map { std =>
      Ok(std.email + " is found!!")
    } getOrElse {
      NotFound("Mail not found :(")
    }
  }

  def findByName(name: String) = Action.async { implicit req =>
    myRepo.findByName(name) map { std =>
      Ok(std.name + " is found!!")
    } getOrElse {
      NotFound("Name not found :(")
    }
  }
}